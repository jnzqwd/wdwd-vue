const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: '//at.alicdn.com/t/font_951447_kz3pe2p79cl.css'
      },
      {
        rel: 'stylesheet',
        href: '/element-ui/lib/theme-chalk/index.css'
      },
      {
        rel: 'stylesheet',
        href: '/avue/lib/index.css'
      }
    ],
    script: [{ src: '/jquery/3.4.1/jquery.min.js' },{ src: '/push/1.0.9/push.min.js' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui',
    { src: '@/plugins/vcharts', ssr: false },
    { src: '@/plugins/avue', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        cacheGroups: {
          'vendor-pageA': {
            // test: /vue/, // 直接使用 test 来做路径匹配
            chunks: 'initial',
            name: 'vendor-pageA',
            enforce: true,
            minSize: 10000, // This is example is too small to create commons chunks,
            maxSize: 3000000
          }
        }
      },
      runtimeChunk: {
        name: 'manifest'
      }
    }
  }
}
