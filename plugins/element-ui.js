import Vue from 'vue'
import Element from 'element-ui'
import WdPluginJsx from '../componentsUI/Index.jsx'
import WdPluginVue from '@/components/index.js'
// import Avue from '@smallwei/avue'
export default () => {
  Vue.use(Element)
  // Vue.use(AVUE)
  Vue.use(WdPluginJsx)
  Vue.use(WdPluginVue)
}
