import Logo from './Logo.vue'
export default {
  install: (Vue, options) => {
    Vue.component('Logo', Logo)
  }
}
