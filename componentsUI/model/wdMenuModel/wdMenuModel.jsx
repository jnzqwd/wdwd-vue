import './wdMenuModel.scss'
export default {
  install: (Vue, options) => {
    Vue.component('wdMenuModel', {
      props: {
        isCollapse: {
          type: Boolean,
          default: true
        },
        menus: {
          type: Array,
          default: []
        }
      },
      render() {
        let submenu = null
        const recurrence = (item, i) => {
          if (!item.children || item.children.length === 0) {
            submenu = (
              <el-menu-item index={item.url}>
                <i class={item.icon} />
                <span slot="title">{item.name}</span>
              </el-menu-item>
            )
          }
          if (item.children && item.children.length > 0) {
            const itemChildren = item.children.map(it => recurrence(it))
            submenu = (
              <el-submenu index={`${Date.now()}`}>
                <template slot="title">
                  <i class={item.icon} />
                  <span slot="title">{item.name}</span>
                </template>
                {itemChildren}
              </el-submenu>
            )
          }
          return submenu
        }
        const menus = this.menus.map((item, i) => recurrence(item))
        return (
          <el-scrollbar
            style={`height:100%`}
            class={`wdMenuModel ${this.isCollapse ? 'all' : ''}`}
          >
            <el-menu
              default-active={this.$route.path}
              router
              collapse={this.isCollapse}
              class="wdMenuBox"
            >
              {menus}
            </el-menu>
          </el-scrollbar>
        )
      }
    })
  }
}
