import './wdLayoutModel.scss'
export default {
  install: (Vue, options) => {
    Vue.component('wdLayoutModel', {
      props: {
        isHeaderShow: {
          type: Boolean,
          default: true
        },
        isMenuShow: {
          type: Boolean,
          default: true
        },
        user: {
          type: Object,
          default: () => ({
            id: 0,
            name: '用户'
          })
        },
        menus: {
          type: Array,
          default: () => [
            {
              name: '首页',
              url: '/',
              icon: 'el-icon-tickets'
            },
            {
              name: '表单',
              url: '/form',
              icon: 'el-icon-tickets'
            },
            {
              name: '表格',
              url: '/table',
              icon: 'el-icon-tickets'
            },
            {
              name: '地图',
              url: '/map',
              icon: 'el-icon-tickets'
            },
            {
              name: '代码生成器',
              url: '/generator',
              icon: 'el-icon-tickets'
            },
            {
              icon: 'el-icon-tickets',
              name: '导航一',
              children: [
                {
                  name: '测试',
                  url: '/test',
                  icon: 'el-icon-tickets'
                },
                {
                  icon: 'el-icon-tickets',
                  name: '导航一',
                  children: [
                    {
                      name: '测试',
                      url: '/test',
                      icon: 'el-icon-tickets'
                    }
                  ]
                }
              ]
            }
          ]
        }
      },
      data() {
        return {
          isCollapse: true,
          headerMenus: [
            {
              url: 'https://gitee.com/jnzqwd/wdwd',
              name: '项目spa版源代码',
              isOuterChain: true
            },
            {
              url: 'https://gitee.com/jnzqwd/wdwd-vue',
              name: '项目ssr版源代码',
              isOuterChain: true
            }
          ]
        }
      },
      watch: {
        isCollapse(v) {
          this.$nextTick(_ => {
            this.$emit('collapse', v)
          })
        }
      },
      mounted() {
        $(window)
          .resize(() => this.resize())
          .resize()
      },
      methods: {
        resize() {
          this.isCollapse = window.innerWidth <= 1360 ? true : false
        }
      },
      render() {
        let headerModel
        let menuModel
        if (this.isHeaderShow) {
          headerModel = (
            <wd-header-model
              is-collapse={this.isCollapse}
              data={this.headerMenus}
              user={this.user}
              {...{
                on: { 'update:isCollapse': val => (this.isCollapse = val) }
              }}
            />
          )
        }
        if (this.isMenuShow) {
          menuModel = (
            <wd-menu-model is-collapse={this.isCollapse} menus={this.menus} />
          )
        }
        return (
          <div id="wdLayoutModel">
            {headerModel}
            <el-container>
              {menuModel}
              <div class={`contextPage ${this.isCollapse ? 'pagemax' : ''}`}>
                <el-scrollbar style="height: 100%">
                  {this.$slots.default}
                </el-scrollbar>
              </div>
            </el-container>
          </div>
        )
      }
    })
  }
}
