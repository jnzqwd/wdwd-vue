import './wdHeaderModel.scss'
export default {
  install: (Vue, options) => {
    Vue.component('wdNav', {
      props: {
        data: {
          type: Array,
          default: () => []
        },
        user: {
          type: Object,
          default: () => ({
            id: 0,
            name: '用户名'
          })
        }
      },
      methods: {
        updateUser() {
          this.$emit('updateUser', this.user)
        },
        exit() {
          this.$emit('exit', this.user)
        }
      },
      render() {
        const dataDoms =
          this.data || this.data.length > 0
            ? this.data.map(item => {
                return item.isOuterChain ? (
                  <li>
                    <a href={item.url} target="_blank">
                      {item.name}
                    </a>
                  </li>
                ) : (
                  <li>
                    <nuxt-link to={item.url} replace>
                      {item.name}
                    </nuxt-link>
                  </li>
                )
              })
            : ''
        return (
          <nav>
            <ul>
              {dataDoms}
              <li>
                <el-dropdown trigger="click">
                  <span class="el-dropdown-link">
                    <no-ssr>
                      <avue-avatar style="color: #f56a00; background-color: #fde3cf">
                        U
                      </avue-avatar>
                    </no-ssr>
                    <span class="userName">{this.user.name}</span>
                  </span>
                  <el-dropdown-menu slot="dropdown">
                    <el-dropdown-item on-click={this.updateUser}>
                      修改信息
                    </el-dropdown-item>
                    <el-dropdown-item on-click={this.exit}>
                      退出
                    </el-dropdown-item>
                  </el-dropdown-menu>
                </el-dropdown>
              </li>
            </ul>
          </nav>
        )
      }
    })
    Vue.component('wdHeaderModel', {
      // eslint-disable-next-line vue/require-prop-types
      props: ['isCollapse', 'data', 'user', 'isHideCollapse'],
      methods: {
        onShowMenu() {
          this.$emit('update:isCollapse', !this.isCollapse)
        },
        onUpdateUser() {
          $emit('updateUser')
        },
        onExit() {
          $emit('exit')
        }
      },
      render() {
        const collapseButton = this.isHideCollapse ? (
          ''
        ) : (
          <i
            class={`showMenu iconfont icon-caidan ${
              this.isCollapse ? 'rotate' : ''
            }`}
            on-click={this.onShowMenu}
          />
        )
        return (
          <header class="wdHeaderModel">
            <div class="header-wrapper">
              <div class="logo">
                <i class="iconfont icon-wdwd" />
                {collapseButton}
              </div>
              <wd-nav
                data={this.data}
                user={this.user}
                on-updateUser={this.onUpdateUser}
                on-exit={this.onExit}
              />
            </div>
          </header>
        )
      }
    })
  }
}
